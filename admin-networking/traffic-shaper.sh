#!/bin/bash

# Script to control the outgoing bandwidth consumed by NXLog, to protect important payload on the network.
# You can use this script as a start point to make a traffic shaper for your host.
# NOTE: add it to boot runlevel

# output network interface
# TODO: change it
DEV=enp0s8
# full rate
FULL_RATE=10000mbit
# limit rate
LIMIT_RATE=1mbit
# limit destination port (on the server). See example below. 
#LIMIT_DPORT=8999

# mark handle (you can add this mark by iptables)
handle=12
# group id
gid=$(id --group nxlog)

# macros
TC=/sbin/tc
TCP="match ip protocol 6 0xff"
UDP="match ip protocol 17 0xff"
DPORT="match ip dport"
SPORT="match ip sport"
SRC="match ip src"
DST="match ip dst"
MARK="match mark"
U32="protocol ip u32"

# clear
echo "clear"
$TC qdisc del dev $DEV root 2>&1 > /dev/null
iptables -t mangle -F PREROUTING 2>&1 > /dev/null
if [ "$1" == "clear" ]; then exit; fi

# mark packets by iptables

# mark by destination port
#iptables -A PREROUTING -t mangle --dport $LIMIT_DPORT -m mark --mark $handle
# mark by user group id
iptables -A OUTPUT -t mangle -m owner --gid-owner $gid -j MARK --set-mark $handle


# create root disc with full RATE (unlimit) 
$TC qdisc add dev $DEV root handle 1: htb
$TC class add dev $DEV parent 1: classid 1:1 htb rate $FULL_RATE
   
  echo "Limit traffic"
  # create limited disc
  $TC class add dev $DEV parent 1:1 classid 1:12 htb rate $LIMIT_RATE
  $TC qdisc add dev $DEV parent 1:12 handle 12: sfq perturb 10
       # filter by $DPORT 
       #$TC filter add dev $DEV parent 1:0 prio 1 $U32 $TCP $DPORT $LIMIT_DPORT 0xffff classid 1:12
       # filter by $MARK
       $TC filter add dev $DEV parent 1:0 prio 1 $U32 $MARK $handle 0xffff classid 1:12 
