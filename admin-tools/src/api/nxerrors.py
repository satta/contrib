

class ActionError(Exception):

    MISSED = 1
    EXISTS = 2
    HTTP = 3
    TIMEOUT = 4
    CONNECTION = 5
    OFFLINE = 6
    NOT_IMPLEMENTED = 7
    SYSTEM = 255

    def __init__(self, num, reason=''):
        self.num = num
        self.reason = reason

    def __str__(self):
        return "Action error {}({}): {}".format(self.num,
                                                ActionError.ERRCODES[self.num],
                                                self.reason)


class Missed(ActionError):

    def __init__(self, objtype, objname):
        ActionError.__init__(self, ActionError.MISSED,
                             "{} '{}' not found".format(objtype, objname))


class NotDefined(ActionError):

    def __init__(self, objtype, objname):
        ActionError.__init__(self, ActionError.MISSED,
                             "{} '{}' not defined".format(objtype, objname))


class Offline(ActionError):

    def __init__(self, ip):
        ActionError.__init__(self, ActionError.OFFLINE,
                             "{} is offline".format(ip))


class Exists(ActionError):

    def __init__(self, objtype, objname):
        ActionError.__init__(self, ActionError.EXISTS,
                             "{} '{}' allready exists".format(objtype, objname))


class RequestError(ActionError):

    def __init__(self, code, error):
        ActionError.__init__(self, ActionError.HTTP,
                             "Request returns error {} ({})".format(code, error))


class ReplyError(ActionError):

    def __init__(self, side, reason):
        ActionError.__init__(self, ActionError.HTTP,
                             "Agent ({}) returns error: {}".format(side, reason))


class RequestTimeout(ActionError):

    def __init__(self, remoteaddr):
        ActionError.__init__(self, ActionError.TIMEOUT,
                             'Connection "{}" timeout'.format(remoteaddr))


class NotImplemented(ActionError):

    def __init__(self, func):
        ActionError.__init__(self, ActionError.NOT_IMPLEMENTED,
                             'Function "{}" not implemented yet'.format(func))


class ConnectionError(ActionError):

    def __init__(self, remoteaddr, reason):
        ActionError.__init__(self, ActionError.CONNECTION,
                             'Connection to "{}" error: {}'.format(remoteaddr, reason))
