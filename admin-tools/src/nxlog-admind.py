#!/usr/bin/python
"""

nxlog manager service

"""

import argparse
import asyncore
import logging
import sys
import api
import core
from api.daemon import Daemon
import api.config
import os


class MyDaemon(Daemon):

    def __init__(self):
        Daemon.__init__(self, api.config.file.pidfile,
                        stdout=api.config.file.daemon_log, stderr=api.config.file.daemon_err_log)

    def run(self):
        logging.basicConfig(filename=api.config.file.logfile,
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        logging.debug('running')
        worker = core.Core()
        while not worker.need_exit:
            asyncore.loop(count=1, timeout=10.0)
        worker.exit()


def parse_args():
    parser = argparse.ArgumentParser(description='nxlog manager service')
    parser.add_argument('command', choices=['start', 'stop', 'restart', 'status'],
                        help='command', nargs='?')
    parser.add_argument('--config', nargs=1, help='config file (optional)')
    return parser.parse_args()

def check_dir(path):
    dir = os.path.dirname(path)
    
    if not os.path.exists(dir):
        print ("Directory '%s' doesn't exist" % dir)
        return False
    return True

def touch(path):
    if not check_dir(path):
        return False

    try:
        with open(path, 'a'):
            os.utime(path, None)
    except IOError as err:
        print("Couldn't create file '%s': %s" % path % err.strerror)
        return False
    return True



if __name__ == "__main__":

    args = parse_args()
    api.config.reload_init(args.config)
    daemon = MyDaemon()


    if not touch(api.config.file.logfile) \
        or not check_dir(api.config.file.pidfile) \
        or not touch(api.config.file.daemon_log) \
        or not touch(api.config.file.daemon_err_log):
        print("Couldn't start daemon")
        exit(1)

    print("logging into %s" % api.config.file.logfile)
    if not args.command or args.command == 'start':
        # try to find all files: log
        try:
            daemon.start()
        except IOError as err:
            sys.stderr.writelines([err.strerror])
            exit(1)
            
    elif args.command == 'stop':
        daemon.stop()
    elif args.command == 'restart':
        daemon.restart()
    elif args.command == 'status':
        if daemon.is_running():
            print('nxlog-admin is running, pid: {}'.format(daemon.pid))
            exit(0)
        else:
            print('nxlog-admin is stoped')
            exit(1)
