#!/usr/bin/python
import argparse
import os
import socket
import json
import api.config

# need to implement socket json reader


def json_read(sock):

    def parse_result(data):
        SUCCESS = 0
        FAIL = 1
        if "result" in data:
            if data['result'] == 'success':
                return SUCCESS
            elif "errnum" in data:
                return int(data['errnum'])
            else:
                return FAIL
        else:
            return FAIL

    parsed = False
    str_json = ''
    json_obj = None
    while not parsed:
        chunk = sock.recv(1024)
        str_json += chunk
        try:
            json_obj = json.loads(str_json)
            parsed = True
        except:
            # not parsed
            pass

    return json_obj, parse_result(json_obj)


def parse_args():
    # first argument is func
    parser = argparse.ArgumentParser()
    parser.add_argument("func",
                        help="action to execute, type 'list' for all functions list ")
    parser.add_argument("--output", '-o', help='output type: json|text',
                        choices=['json', 'text'])
    parser.add_argument("--delim", '-d',
                        help='column delimiter in text mode. default is " "',
                        default=' ')
    parser.add_argument("--align", '-a',
                        help='text fields aligment. default is "off"',
                        choices=['off', 'left', 'right', 'center'], default='off')
    parser.add_argument("--config", '-c',
                        help='config file (default ./nxlog-admin.conf.json)')

    known, unknown = parser.parse_known_args()
    # split unknown to name=value pairs
    func = known.func
    params = {"func": func, "cwd": os.getcwd()}
    unknown = filter(lambda x: x.find('=') != -1, unknown)
    for unknown_arg in unknown:
        name, value = unknown_arg.split('=')
        params[name] = value
    output_method = 'json'
    if not known.output is None:
        output_method = known.output

    return (params, output_method, known.align, known.delim, known.config)


def format_line(values, align=None, delim=' '):
    # values is [(value, max_len)]
    stralign = '<'
    if align == 'left':
        stralign = '<'
    elif align == 'right':
        stralign = '>'
    elif align == 'center':
        stralign = '^'
    if not align or align == 'off':
        strvals = [str(value) for (value, max_len) in values]
    else:
        strvals = [('{:' + stralign + str(max_len) + '}').format(value)
                   for (value, max_len) in values]
    return delim.join(strvals)


def formatter(type, indent=4, align=None, delim=' '):
    """ decorator maker for output format """

    def json_formatter(func):
        """ format output as json """
        def wrapped():
            input_dict, result = func()
            return json.dumps(input_dict, indent=indent), result
        return wrapped

    def text_formatter(func):
        """ format output as simple text """
        def wrapped():
            input_dict, result = func()
            rows = []
            if 'elements' in input_dict and len(input_dict['elements']) > 0:
                elements = input_dict['elements']
                max_lens = {}
                for el in elements:
                    for name, val in el.iteritems():
                        if name not in max_lens:
                            max_lens[name] = len(str(val))
                        else:
                            max_lens[name] = max(
                                max_lens[name], len(str(val)))
                for el in elements:
                    values = []
                    for name, val in el.iteritems():
                        values.append((val, max_lens[name]))
                    string = format_line(values, align=align, delim=delim)
                    rows.append(string)

            else:
                for name, val in input_dict.iteritems():
                    if name != "elements":
                        rows.append("{}{}{}".format(name, delim, val))

            return "\n".join(rows), result
        return wrapped
    return text_formatter if type == 'text' else json_formatter


query, output, oalign, odelim, config = parse_args()

api.config.reload_init(config)


@formatter(type=output, align=oalign, delim=odelim)
def request():
    socket_file = api.config.file.ctls[0].file

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(socket_file)
    sock.send(json.dumps(query))

    # wait json
    data, result = json_read(sock)
    return data, result

    # todo: return errorcode

outstr, result = request()
print(outstr)
exit(result)
