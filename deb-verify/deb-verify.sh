#!/bin/bash

# deb sign verification script
P=$(dirname $0)

# verify:
if [ -f "$1" ]; then
   debsig-verify --policies-dir $P/policies/ --keyrings-dir $P/keyrings/ "$1"

# or install politics and keyrings
elif [ "x$1" == "x-i" ]; then
  if [ `whoami` != "root" ]; then
    echo "su required for install"
    exit 1
  fi
  cp -vR $P/policies/* /etc/debsig/policies/
  cp -vR $P/keyrings/* /usr/share/debsig/keyrings/
 
# default action
else
  echo -e "Use: \n\
  $0 -i  : to install policies and keyrings to system \n\
  $0 package.deb  : to verify package without installation"
fi
