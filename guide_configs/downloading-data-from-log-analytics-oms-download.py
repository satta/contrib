import datetime
import json
import requests

import adal
import nxlog

class LogReader:

    def __init__(self, time_interval):
        # Details of workspace.  Fill in details for your workspace.
        resource_group = '<YOUR_RESOURCE_GROUP>'
        workspace = '<YOUR_WORKSPACE>'

        # Details of query.  Modify these to your requirements.
        query = "Type=*"
        end_time = datetime.datetime.utcnow()
        start_time = end_time - datetime.timedelta(seconds=time_interval)
        num_results = 100000  # If not provided, a default of 10 results will be used.

        # IDs for authentication.  Fill in values for your service principal.
        subscription_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
        tenant_id = 'xxxxxxxx-xxxx-xxxx-xxx-xxxxxxxxxxxx'
        application_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx'
        application_key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

        # URLs for authentication
        authentication_endpoint = 'https://login.microsoftonline.com/'
        resource  = 'https://management.core.windows.net/'

        # Get access token
        context = adal.AuthenticationContext('https://login.microsoftonline.com/' + tenant_id)
        token_response = context.acquire_token_with_client_credentials('https://management.core.windows.net/', application_id, application_key)
        access_token = token_response.get('accessToken')

        # Add token to header
        headers = {
            "Authorization": 'Bearer ' + access_token,
            "Content-Type":'application/json'
        }

        # URLs for retrieving data
        uri_base = 'https://management.azure.com'
        uri_api = 'api-version=2015-11-01-preview'
        uri_subscription = 'https://management.azure.com/subscriptions/' + subscription_id
        uri_resourcegroup = uri_subscription + '/resourcegroups/'+ resource_group
        uri_workspace = uri_resourcegroup + '/providers/Microsoft.OperationalInsights/workspaces/' + workspace
        uri_search = uri_workspace + '/search'

        #store log data for NXLog here
        self.lines = ""

        # Build search parameters from query details
        search_params = {
                "query": query,
                "top": num_results,
                "start": start_time.strftime('%Y-%m-%dT%H:%M:%S'),
                "end": end_time.strftime('%Y-%m-%dT%H:%M:%S')
                }

        # Build URL and send post request
        uri = uri_search + '?' + uri_api
        response = requests.post(uri,json=search_params,headers=headers)

        # Response of 200 if successful
        if response.status_code == 200:

            # Parse the response to get the ID and status
            data = response.json()
            search_id = data["id"].split("/")
            id = search_id[len(search_id)-1]
            status = data["__metadata"]["Status"]

            # If status is pending, then keep checking until complete
            while status == "Pending":

                # Build URL to get search from ID and send request
                uri_search = uri_search + '/' + id
                uri = uri_search + '?' + uri_api
                response = requests.get(uri,headers=headers)

                # Parse the response to get the status
                data = response.json()
                status = data["__metadata"]["Status"]
        else:

        # Request failed
            print (response.status_code)
            response.raise_for_status()

        print ("Total records:" + str(data["__metadata"]["total"]))
        print ("Returned top:" + str(data["__metadata"]["top"]))

    #write a JSON dump of all events
	for event in data['value']:
            self.lines += json.dumps(event) + '\n'

    def getlogs(self):
        if not self.lines:
            return None
        return self.lines

def read_data(module):
    # log pull time interval in seconds
    time_interval = 300

    module['reader'] = LogReader(time_interval)
    reader = module['reader']
    logdata = module.logdata_new()
    line = reader.getlogs()

    if line:
        logdata.set_field('raw_event', line)
        logdata.post()
        nxlog.log_debug("Data posted")

    module.set_read_timer(time_interval)

nxlog.log_info("INIT SCRIPT")