# A regular expression for parsing headers
define SARA_HEAD_RE /(?x)^\**\s+(?<Server>\w+).\s+(?<Message>[\w\s]*)\**\s+\w+.\
                    \s+(?<TraceDate>\d+.\d+.\d+)/

# A regular expression for parsing the first type of messages
define SARA_REGEX_1 /(?x)^(?<EventTime>\d+.\d+.\d+)[\d\w|*.]*\s+\
                    (?<EventType>\w+)\:(?<Message>.*)/

# A regular expression for parsing the second type of messages
define SARA_REGEX_2 /(?x)^(?<EventTime>\d+.\d+.\d+)[\d\w|*.]*\s+(?<Message>.*)/

# Generic path to the folder with log files
define PQ_PATH      C:\ProgramData\Siemens Energy\SICAM PQ Analyzer\Logs

<Extension json>
    Module          xm_json
</Extension>

<Extension multiline_sara>
    Module          xm_multiline

    # A regular expression for identifying message headers
    HeaderLine      /^\*\*\s+\w+\:\s[\w\s]*/

    # A regular expression for identifying message ends
    EndLine         /^\*\*\s+\w+\:\s\d+.\d+.\d+/
</Extension>

<Input from_file_header>
    Module          im_file
    File            '%PQ_PATH%\SARAServer\Demo.Log'
    InputType       multiline_sara
    <Exec>

        # Replacing unwanted characters
        $raw_event = replace($raw_event, "\r\n", "");

        # Matching events to the regular expression and converting to JSON
        if $raw_event =~ %SARA_HEAD_RE% to_json();

        # Discarding messages
        else drop();
    </Exec>
</Input>

<Input from_file>
    Module          im_file
    File            '%PQ_PATH%\SARAServer\Demo.Log'
    <Exec>

        # Matching events to the regular expressions and converting to JSON
        if ($raw_event =~ %SARA_REGEX_1%) or
           ($raw_event =~ %SARA_REGEX_2%) to_json();

        # Discarding messages
        else drop();
    </Exec>
</Input>