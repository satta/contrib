param (
    [int]$Interval = 10
)

Try {
    while($true) {
        $ConnectionSummary = Get-RemoteAccessConnectionStatisticsSummary

        foreach ($itm in $ConnectionSummary.CimInstanceProperties) {
            if ([string]::IsNullOrWhiteSpace($itm.Value)) {
                $itm.Value = 0
            }
        }

        $ActiveConnections = @()

        foreach ($connection in Get-RemoteAccessConnectionStatistics) {
            $ActiveConnections += @{
                    ClientIPAddress = $connection.ClientIPv4Address.IPAddressToString
                    HostName = $connection.HostName
                    UserName = $connection.UserName[0]
                    ConnectionState = $connection.UserActivityState
                    ConnectionDuration = $connection.ConnectionDuration
                    ConnectionType = $connection.ConnectionType
                    AuthenticationMethod = $connection.AuthMethod
                    ConnectionStartTime = $connection.ConnectionStartTime.DateTime
                    TotalBytesIn = $connection.TotalBytesIn
                    TotalBytesOut = $connection.TotalBytesOut
            }
        }

        $event = @{
            TotalConnections = $ConnectionSummary.TotalConnections
            TotalDAConnections = $ConnectionSummary.TotalDAConnections
            TotalVpnConnections = $ConnectionSummary.TotalVpnConnections
            TotalUniqueUsers = $ConnectionSummary.TotalUniqueUsers
            MaxConcurrentConnection = $ConnectionSummary.MaxConcurrentConnections
            TotalCumulativeConnections = $ConnectionSummary.TotalCumulativeConnections
            TotalBytesIn = $ConnectionSummary.TotalBytesIn
            TotalBytesOut = $ConnectionSummary.TotalBytesOut
            TotalBytesInOut = $ConnectionSummary.TotalBytesInOut
            ActiveConnections = $conns
        }

        Write-Output $event | ConvertTo-JSON -Compress
        Start-Sleep -s $Interval
    }
}
Catch {
    Write-Error "An unhandled exception occurred!"
    exit 1
}