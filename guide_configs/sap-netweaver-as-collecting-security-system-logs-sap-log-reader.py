# Copyright © NXLog Ltd. All Rights Reserved.
#
# This software is the proprietary product of NXLog Ltd and NXLog Ltd
# retains all copyright and ownership interest in the software.
#
# This file is part of the nxlog log collector tools.
# For licensing terms questions please contact support@nxlog.org.
# Website: http://nxlog.org
# Author: Seth Stenzel <seth.stenzel@nxlog.org>


import os
import sys
import glob
import time


#event character length for Security Audit Logs
event_length = 200

#event character length for System Logs
#event_length = 320

try:
    if len(sys.argv) <2:
        raise Exception("No directory argument provided.")

    # directory to monitor
    directory = str(sys.argv[1])

    previous_file = ''
    previous_size = 0
    new_bytes = 0

    while True:
        try:
            # get most recent file which contains name part
            # if needing a specific file ext then add ext after '\*' (ex: '\*.csv')
            dir_files = glob.glob(directory + '/audit_*')
            if len(dir_files) > 0:
                latest_file = max(dir_files, key=os.path.getmtime)
            else:
                time.sleep(0.01)
                continue

            # check if most recent event file name has changed
            if latest_file != '' and previous_file != '' and latest_file != previous_file:
            # checks if there is still new data to be read from previous file when new latest file is found
            # this is rough handling with the assumption that new files could not be made so quickly
            # that between one iteration, and another that multiple new files will not be created
                if previous_size == os.stat(previous_file).st_size:
                    previous_size = 0
                    previous_file = latest_file
                else:
                    latest_file = previous_file

            # get the current event file size
            latest_size = os.stat(latest_file).st_size

            # if empty or unchanged, sleep for 10ms then go to next loop interation
            if latest_size == 0 or latest_size == previous_size:
                time.sleep(0.01)
                continue
            # else if file has content and has grown, parse new events
            elif latest_size != previous_size and latest_size > previous_size:
                new_bytes = latest_size - previous_size

            # open the file with context manager
                with open(latest_file, 'rb') as lf:
                    # seeks to the point in the file where new bytes should start instead of reading whole file
                    lf.seek(latest_size - new_bytes)
                    # reads from seek point for the length of new_bytes
                    new_events = lf.read(new_bytes)
                    # decodes utf-16 data
                    new_events = new_events.decode('utf-16')
                    # splits the string at n length where n length is the variable event_length at the top of script;
                    new_events_list = [new_events[i:i+event_length] for i in range(0, len(new_events), event_length)]
            # print each new event
                for event in new_events_list:
                    print(event.replace("\n", " "))
                previous_file = latest_file
                previous_size = latest_size
            else:
                print("WARNING - FILE TRUNCATION DETECTED")
                previous_file = latest_size
        except Exception as e:
            print(f"ERROR - ERROR PARSING FILE ({e})", repr(e))
except Exception as e:
    print(f"ERROR - ERROR ({e})", repr(e))