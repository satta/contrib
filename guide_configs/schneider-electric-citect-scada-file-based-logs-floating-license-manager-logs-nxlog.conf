# A regular expression defined as a constant to read the content of the logs
define FLMRG_REGEX  /(?x)^(?<EventTime>\d+-\d+-\d+,\d+:\d+:\d+:\d+),\
                    (?<ServerName>\w+),(?<Severity>\w+),\
                    \s(?<EventGroupLineID>150),\s(?<EvetGroupID>\d+),\-+\s\d+.\
                    \d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\s+(?<InfoTypeLineID>\d+)\
                    ,\s+\d+,(?<InfoType>\w+\s+\w+):\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\
                    \d*,\w+,\w+,\s+(?<ComputernameLineID>\d+),\s+\d+,\w+\s+\w+:\
                    \s+(?<ComputerName>\w+.+)\s\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\
                    \w+,\w+,\s+(?<UserNameLineID>\d+),\s+\d+,\w+\s+\w+:\
                    \s+(?<UserName>\w+)\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\
                    \s+(?<OSVersionLineID>\d+),\s\d+,\w+:\s+(?<OSVersion>\w+\s+\
                    \(\w+\s\d+\),\s\d+-\w+)\s\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\
                    \w+,\s+(?<HypervisorLineID>\d+),\s+\d+,\w+:\s+(?<Hypervisor>\
                    \w+.\w+|\w+)\s\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\
                    \s+(?<ProductLineID>\d+),\s+\d+,\w+:\s+(?<Product>\w+\s\w+\s\
                    \w+\s\d+.\d+)\s\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\
                    \s(?<LogLevelLineID>\d+),\s\d+,\w+\s\w+:\s+(?<LogLevel>\d+)\s+\
                    \d+.\d+.\d+\d+.\d+.\d+.\d+.\d+,\w+,\w+,\
                    \s+(?<FNPAPIVersionLineID>\d+),\s+\d+,\w+.\w+.\w+:\
                    \s+(?<FNPAPIVersion>v\d+.\d+.\d+.\d+\s\w+\s\d+\s\w+)\s\d+.\d+.\
                    \d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\
                    \s+(?<LicensingServiceVersionLineID>\d+),\s+\d+,\w+.\w+.\w+:\
                    \s+(?<LicensingServiceVersion>v\d+.\d+.\d+.\d+\s\w+\s\d+\s\d+\
                    \/\d+\/\d+)\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\w+,\
                    \s(?<SrvActBrickdllVersionLineID>\d+),\s\d+,\w+.\w+\s+\w+:\
                    \s+(?<SrvActBrickdllVersion>\d+.\d+.\d+.\d+)\s+\d+.\d+.\d+\d+.\
                    \d+.\d+.\d+.\d*,\w+,\w+,\s(?<SrvActBrickXdllVersionLineID>\
                    \d+),\s\d+,\w+.\w+\s+\w+:\s+(?<SrvActBrickXdllVersion>\d+.\
                    \d+.\d+.\d+\s+\w+\s+\d+)\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\
                    \w+,\w+,\s+(?<FnpCommsSoapdllVersionLineID>\d+),\s+\d+,\w+.\
                    \w+\s+\w+:\s+(?<FnpCommsSoapdllVersion>\d+.\d+.\d+.\d+\s+\w+\
                    \s+\d+)\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\d+,\w+,\w+,\s+\d+,\s+\
                    \d+,\-+\s+\d+.\d+.\d+\d+.\d+.\d+.\d+.\d*,\w+,\
                    \w+,(?<Message1LineID>\d+),\s\d+,(?<Message1>\w+\s\w+\s\w+\s\
                    \w+\s\w+\s\w+)\s\d+.\d+.\d+\d+.\d+.\d+.\d+.\d+,\w+,\w+,\
                    \s+(?<Message2LineID>\d+),\s+\d+,(?<Message2>.*)/

# Part of the log path defined as a constant
define FLMRG_PATH   C:\Users\<USERNAME>\AppData\Local\Temp

<Extension json>
    Module    xm_json
</Extension>

<Extension multiline>
    Module        xm_multiline
    # Regular expression to look for the header of the message
    HeaderLine    /(\d+-\d+-\d+),(\d+:\d+:\d+:\d+),(\w+),INF,\s+150(.*)/
</Extension>

<Input from_file>
    Module        im_file
    File          '%FLMRG_PATH%\Floating License Manager_reg.log'
    # Defines that the input has to be first paresd with the regular
    # expression in the multiline extension module
    InputType     multiline
    <Exec>
    # Replaces unwanted characters
    $raw_event = replace($raw_event, "\r", "");
    $raw_event = replace($raw_event, "\n", " ");
    $raw_event = replace($raw_event, "\t", " ");
    </Exec>
    <Exec>
    # Matches the events with a regular expression
    if $raw_event =~ %FLMRG_REGEX%
    {
        # Creates the timestamp
        $EventTime = strptime($EventTime, "%m-%d-%Y,%H:%M:%S");
        # Saves the LineID fields as integers (default is string)
        $EventGroupLineID = integer($EventGroupLineID);
        $EvetGroupID = integer($EvetGroupID);
        $InfoTypeLineID = integer($InfoTypeLineID);
        $ComputernameLineID = integer($ComputernameLineID);
        $UserNameLineID = integer($UserNameLineID);
        $OSVersionLineID = integer($OSVersionLineID);
        $HypervisorLineID = integer($HypervisorLineID);
        $ProductLineID = integer($ProductLineID);
        $LogLevelLineID = integer($LogLevelLineID);
        $FNPAPIVersionLineID = integer($FNPAPIVersionLineID);
        $LicensingServiceVersionLineID = integer($LicensingServiceVersionLineID);
        $SrvActBrickdllVersionLineID = integer($SrvActBrickdllVersionLineID);
        $SrvActBrickXdllVersionLineID = integer($SrvActBrickXdllVersionLineID);
        $FnpCommsSoapdllVersionLineID = integer($FnpCommsSoapdllVersionLineID);
        $Message1LineID = integer($Message1LineID);
        $Message2LineID = integer($Message2LineID);
        # Formats the result as JSON
        to_json();
    }
    </Exec>
</Input>