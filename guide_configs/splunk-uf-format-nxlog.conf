<Extension Drop_Fields>
    Module    xm_rewrite
    Keep      # Remove all
</Extension>

<Input DNS_Server_Audit>
    Module  im_msvistalog
    File %SystemRoot%\System32\Winevt\Logs\Microsoft-Windows-DNSServer%4Audit.evtx
    <Exec>
        # Create a header variable for storing the Splunk datetime string
        create_var('timestamp_header');
        create_var('event');            # The Splunk equivalent of a $raw_event
        create_var('message');          # For preserving the $Message field
        create_var('vip_fields');       # Message subfields converted to fields

        # Get the Splunk datetime string needed for the Header Line
        $dts = strftime($EventTime,'YYYY-MM-DD hh:mm:ss.sTZ');
        $hr = "";  # Hours, 2-digit
        $ap = "";  # For either "AM" or "PM";
        if ($dts =~ /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/ ) {
            if (hour($EventTime) < 12) {
                $ap = "AM";
                $hr = $4;
                if (hour($EventTime) == 0) $hr = "12";
            }
            if (hour($EventTime) > 11) {
                $ap = "PM";
                if (hour($EventTime) == 12) $hr = $4;;
                if (hour($EventTime) > 12) {
                    $hr = hour($EventTime) - 12;
                    if (hour($EventTime) < 22)  $hr = "0" + $hr;
                }
            }
            $dts = $2 +"/"+ $3 +"/"+ $1 +" "+ \
                   $hr +":"+ $5 +":"+ $6 +" "+ $ap + "\n";
        }
        set_var('timestamp_header', $dts);

        # Convert $EventType to what Splunk expects
        $EventType = ($EventType == "INFO" ? 4 : $EventType);

        # Some really important DNS fields that Splunk doesn't parse
        $vipFields = "";

        if (defined($NAME))
            $vipFields = $vipFields + 'NAME=' + $NAME + "\n";
        if (defined($Severity))
            $vipFields = $vipFields + 'Severity=' + $Severity + "\n";
        if (defined($TTL))
            $vipFields = $vipFields + 'TTL=' + $TTL + "\n";
        if (defined($BufferSize))
            $vipFields = $vipFields + 'BufferSize=' + $BufferSize + "\n";
        if (defined($RDATA))
            $vipFields = $vipFields + 'RDATA=' + $RDATA + "\n";
        if (defined($Zone))
            $vipFields = $vipFields + 'Zone=' + $Zone + "\n";
        if (defined($ZoneScope))
            $vipFields = $vipFields + 'ZoneScope=' + $ZoneScope + "\n";
        set_var('vip_fields', $vipFields);

        # Store and display the original $Message field at the end of the list
        # of fields, just in case Splunk parses it correctly
        set_var('message', $Message);

        # Set the new Splunk Event for DNS Server Audit
        set_var('event', \
            'LogName='      + $Channel         +"\n"+ \
            'SourceName='   + $SourceName      +"\n"+ \
            'EventCode='    + $EventID         +"\n"+ \
            'EventType='    + $EventType       +"\n"+ \
            'Type='         + 'Information'    +"\n"+ \
            'ComputerName=' + $Hostname        +"\n"+ \
            'User='         + 'NOT_TRANSLATED' +"\n"+ \
            'Sid='          + $UserID          +"\n"+ \
            'SidType='      + '0'              +"\n"+ \
            'TaskCategory=' + $Category        +"\n"+ \
            'OpCode='       + $OpCode          +"\n"+ \
            'RecordNumber=' + $RecordNumber    +"\n"+ \
            'Keywords='     + $Keywords        +"\n"  \
        );

        # Remove all NXLog fields.
        # This is necessary for emulating the Splunk proprietary format.
        Drop_Fields->process();

        # Add the Splunk datetime string as a "header" line for this
        # multi-line event
        $raw_event = get_var('timestamp_header') + get_var('event') + \
            get_var('vip_fields') + 'Message=' + get_var('message') +"\n";
    </Exec>
</Input>

<Output Splunk_TCP_DNS_Audit>
    Module  om_tcp
    Host    192.168.1.52
    Port    1515
</Output>