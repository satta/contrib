# Regular expressions defined as a constants to read the content of the logs
define HEADER_REGEX     (?x)^(\d+.\d+.\d+\s*\d+.\d+.\d+.\d+)[\s\:]*
define MESSAGE_REGEX    (?<Message>.*)

define COPY_REGEX       (?<Action>.*?)\s+\"(?<File>.*?)\"\s+\"(?<Destination>.*?)\"\s+(?<Status>.*)
define LAUNCH_REGEX     (?<Action>.*?)\s+\"(?<Exec>.*?)\"\s+(?<IP>\d+\.\d+\.\d+\.\d+)\s+(?<Port>\d+)\s+(?<Status>.*)
define FILEEXITS_REGEX  (?<Action>.*?)\s+\"(?<FilePath>.*?)\"\s+(?<Status>.*)
define EXCEPTION_REGEX  (?<Exception>.*Exception.*?)\:\s+(?<ExceptionComment>.*?)\s+
define OBJNAME_REGEX    Object\s+name[\:\s\']*(?<ObjectName>.*?)[\'\.]*\s+
define ADDINFO_REGEX    (?<AddInfo>at\s+.*)

# Part of the log path defined as a constant
define PE_LOG_PATH      C:\Users\Administrator\AppData\Roaming\Schneider Electric\Process Expert 2020 R2\Logs

<Extension multiline_vm>
    Module        xm_multiline
    # Regular expression to look for the header of the message
    HeaderLine    /\d+.\d+.\d+\s*\d+.\d+.\d+.\d+/
</Extension>

<Extension json>
    Module        xm_json
</Extension>

<Input from_file>
    Module      im_file
    File        '%PE_LOG_PATH%\HybridDCS.Vm.2020R2#0.log'
    InputType   multiline_vm

    <Exec>
        # Replaces unwanted characters
        $raw_event =~ s/\\r\\n/ /g;
        $raw_event =~ s/\s{2,}/ /g;
    </Exec>

    <Exec>
        # Matches the events with a regular expression
        if $raw_event =~ /%HEADER_REGEX% %EXCEPTION_REGEX% %OBJNAME_REGEX% %ADDINFO_REGEX%/ or
        $raw_event =~ /%HEADER_REGEX% %COPY_REGEX%/ or
        $raw_event =~ /%HEADER_REGEX% %LAUNCH_REGEX%/ or
        $raw_event =~ /%HEADER_REGEX% %FILEEXITS_REGEX%/ or
        $raw_event =~ /%HEADER_REGEX% %MESSAGE_REGEX%/
        {
            # Creates the timestamps
            $EventTime = parsedate($1);
            # Formats the result as JSON
            to_json();
        }
        # Discarding messages if not matching expressions
        else drop();
    </Exec>
</Input>
