This script uses the curl utility to send data to an im_http instance. See more details in the official documentation: https://nxlog.co/documentation/nxlog-user-guide/im_http.html
