This script can be used to run Process Governor as a service, without installing Process Governor in debug mode.
It will run as a service using [NSSM](https://nssm.cc).

To install the service, download and run the `installer.ps1` script.
Configuration options can be specified in `processor/procgov.json.`

## How it works

The `installer.ps1` script performs the following tasks:

* Confirms if procgov exists, if it does not it will download and install it.
* Confirms if NSSM exists, if it does not it will download and install it.
* Adds `processor.exe` to the procgov working directory specified in `procgov.json`.
* Creates a JSON file which is used to control `processor.exe`.
* Adds a service through NSSM and starts the service. This can later be controlled through the Windows Services console.

The files taken from the compilation process are also made available in the processor folder as they are needed for the executable to run.
Libraries which are not by default part of Python are included for the executable to be compiled.


This script is provided "AS IS" without warranty of any kind, either expressed or implied. Use at your own risk.
