This batch script configures the Windows audit policy of the local machine as follows:

* On Windows 2008 and newer, it uses [auditpol](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/auditpol) to configure advanced audit policies, enable Global Object Access Auditing for Everyone, and enforce advanced audit policies.
* On older versions of Windows, it uses [secedit](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/secedit) to configure basic audit policies. Note that on machines that have the [Audit: Force audit policy subcategory settings (Windows Vista or later) to override audit policy category settings](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/audit-force-audit-policy-subcategory-settings-to-override) policy enabled, _secedit_ will skip configuration of basic audit policies.

Useful links:

* [Advanced security audit policy settings](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/advanced-security-audit-policy-settings)
* [Basic security audit policy settings](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/basic-security-audit-policy-settings)

This script is provided "AS IS" without warranty of any kind, either expressed or implied. Use at your own risk.